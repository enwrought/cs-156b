""" This file converts the 'all.dta' file into
    the 'base.mm', 'valid.mm', 'hidden.mm' 
    MatrixMarket matrix format.
"""
    

# file locations/constants
data_file = "/dos/Data/UM/um/all.dta"
index_file = "/dos/Data/UM/um/all.idx"
header = "%%MatrixMarket matrix coordinate real general\n"
header2 = "458293 17770 %d\n"

base_file = "/dos/Data/UM/um/base.mm"
valid_file = "/dos/Data/UM/um/valid.mm"
hidden_file = "/dos/Data/UM/um/hidden.mm"

"""
indices = []
h = open(index_file, 'r')
for line in h:
  indices.append(int(line))
h.close()
curr_index = 0
"""

def convert_line(line):
  """ Returns a 4-tuple (user, movie, date, rating)
      from the text line """
  #global curr_index, indices
  text = line.split(' ')
  return map(int, text)


def add_movie(user, movie, date, rating, keys, values):
  """ Adds the movie if the movie is not already in the
      dictionary. """
  if len(keys) == 0 or (user != keys[-1][0] or movie != keys[-1][1]):
      keys.append((user, movie))
      values.append(rating)


def write_dic(location, keys, values):
  g = open(location, 'w')
  g.write(header)
  g.write(header2 % len(keys))
  for i in xrange(len(keys)):
    g.write("%d %d %d\n" % (keys[i][0], keys[i][1], values[i]))
  g.close()




# dictionaries storing the information
# dic[(user, movie)] = (date, rating)
base = []
base_ratings = []
valid = []
valid_ratings = []
hidden = []
hidden_ratings = []
f = open(data_file, 'r')
h = open(index_file, 'r')
count = 0
for line in f:
  if count % 1000000 == 0:
    print "Done reading %d / %d" % (count, 102416306)
  tmp = convert_line(line)
  if len(tmp) != 4:
    continue
  (user, movie, date, rating) = tmp
  dataset = int(h.readline())
  if dataset == 1:
    add_movie(user, movie, date, rating, base, base_ratings)
  elif dataset == 2:
    add_movie(user, movie, date, rating, valid, valid_ratings)
  elif dataset == 3:
    add_movie(user, movie, date, rating, hidden, hidden_ratings)
  count += 1
f.close()
h.close()

print "Done reading data.  Now writing..."


write_dic(base_file, base, base_ratings)
write_dic(valid_file, valid, valid_ratings)
write_dic(hidden_file, hidden, hidden_ratings)
