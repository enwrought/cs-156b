import numpy as np

u_file = open("/dos/Data/UM/um/factorization/041315/base.mm_U.mm", 'r')
u_file.readline() # get rid of MatrixMarket header
(rows_u, cols_u) = map(int, u_file.readline().split(" ")) # get number of rows/cols

u_data = []
# read in data
# MatrixMarket data is column-major
for col in xrange(cols_u):
  curr_col = []
  for row in xrange(rows_u):
    curr_col.append(float(u_file.readline()))
  u_data.append(curr_col)

u_file.close()
#u_mat = np.transpose(np.matrix(u_data))

# repeat the same thing for matrix V
v_file = open("/dos/Data/UM/um/factorization/041315/base.mm_V.mm", 'r')
v_file.readline() # get rid of MatrixMarket header
(rows_v, cols_v) = map(int, v_file.readline().split(" ")) # get number of rows/cols

v_data = []
# read in data
# MatrixMarket data is column-major
for col in xrange(cols_v):
  curr_col = []
  for row in xrange(rows_v):
    line = v_file.readline()
    if len(line) == 0:
      break
    print len(line), line
    curr_col.append(float(line))
  v_data.append(curr_col)

v_file.close()
#v_mat = np.transpose(np.matrix(v_data))
#print v_mat


qual_file = open("/dos/Data/UM/um/qual.dta", 'r')
pred_file = open("/dos/Data/UM/um/pred-041315.dta", 'w')
for line in qual_file:
  (user, movie, date) = map(int, line.split(' '))
  pred = 0.0
  for i in range(5):
    pred += u_data[i][user-1] * v_data[i][movie-1]
  if pred > 5:
    pred = 5.00
  elif pred < 1:
    pred = 1.00
  pred_file.write("%.2f\n" % pred)

qual_file.close()
pred_file.close()



