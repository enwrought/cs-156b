#include <iostream>
#include <vector>

struct Rating {
    int user;
    int movie;
    unsigned short rating;
};



class SparseMatrix {
    std::vector<Rating> ratings;
    
    public:
    SparseMatrix();
    ~SparseMatrix();
    void addRating(int user, int movie, unsigned short rating);
    int get(int user, int movie);
    void sort();
    
    private:
    struct sortstruct {
        bool operator() (const Rating& r1, const Rating& r2) {
            if (r1.user == r2.user)
                return r1.movie < r2.movie;
            else
                return r1.user < r2.user;
        }
    } sortObj;
    bool compare(Rating r, int user, int movie);
};
