#include "Matrix.hh"
#include <iostream>
#include <stdlib.h>
#include <sstream>


Matrix::Matrix(int r, int c) {
    // Initialize variables
    rows = r;
    k = c;
    values = new double*[rows];
    for (int i = 0; i < rows; i++) {
        values[i] = new double[k];
    }
    
    // For right now, we initialize the values to random values
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < k; j++) {
            values[i][j] = (((double)rand()) / RAND_MAX);
        }
    }
}

Matrix::~Matrix() {
    for (int i = 0; i < rows; i++) {
        delete[] values[i];
    }
    delete[] values;
}

double Matrix::frob_norm() {
    // TODO: implement
    return 0.0;
}

void Matrix::update(int row, double* gradient, double lrate) {
    for (int i = 0; i < k; i++) {
        values[row][i] += gradient[i];
    }
}

void Matrix::set(int row, double* value) {
    for (int i = 0; i < k; i++) {
        values[row][i] = value[i];
    }
}

double* Matrix::get_row(int row) {
    return values[row];
}

int Matrix::size() { return rows; }

int Matrix::get_k() { return k; }

std::string Matrix::to_string() {
    std::ostringstream s;
    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < k; col++) {
            s << values[row][col];
            if (col != k - 1)
                s << " ";
        }
        s << "\n";
    }
    return s.str();
}

