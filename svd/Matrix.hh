#include <fstream>
#include <string>

class Matrix {
    int rows, k;
    double** values;
    
    public:
    Matrix(int r, int c);
    ~Matrix();
    double frob_norm();
    void update(int row, double* gradient, double lrate);
    void set(int row, double* value);
    double* get_row(int row);
    int size();
    int get_k();
    std::string to_string();
};
