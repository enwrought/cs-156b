/*
 * svd.cpp
 * 
 * Copyright 2015 Caltech CS156b "Washington Redskins" team
 * Bryant Lin <enwrought>
 * Rohan Batra <rohbat>
 * 
 * 
 */


#include <iostream>
#include <stdlib.h>
#include <assert.h>
#include <fstream>
#include <string>
#include "Matrix.hh"
#include "SparseMatrix.hh"


double predict_rating(int user, int movie, Matrix* U, Matrix* V) {
    int k = U->get_k();
    // assert(k == V.get_k());
    
    double* u_i = U->get_row(user);
    double* v_j = V->get_row(movie);
    
    double returnValue = 0.0;
    
    for (int i = 0; i < k; i++) {
        returnValue += u_i[i] * v_j[i];
    }
    return returnValue;
}

void update_sgd(Matrix* U, Matrix* V, 
                SparseMatrix* data) {
    // Initialize variables
    int num_users = U->size();
    int num_movies = V->size();
    int k = U->get_k();
    
    double eta = 0.01;   // learning rate
    double delta = 0.01; // step size
    
    // check that U.k == V.k
    // assert(k == V.get_k());
    
    
    
    // TODO: maybe parallelize this code
    // Update U first:
    for(int user = 1; user <= num_users; user++) {
        // double* curr_u = U.get(user);
        double* gradient_u = new double[k]();
        // sum up all yij(

        for (int j = 1; j <= num_movies; j++) {
            int yij = data->get(user, j);
            if (yij > 0) {
                // Then yij exists, so let's update
                        std::cout << "Hi!" << std::endl;
                double* v_j = V->get_row(j);
                
                // compute err = y - u_i^T * v_j
                // recall that predict_rating(i,j) = u_i^T * v_j
                double err = yij - predict_rating(user, j, U, V);
                
                // now add to the gradient
                for (int col = 0; col < k; col++) {
                    gradient_u[col] -= 2 * err * delta * v_j[col];
                }
            }
            //std::cout << "Finished " << j << "/" << num_movies << " movies" << std::endl;
        }
        // do update, free gradient_u
        U->update(user, gradient_u, delta);
        delete[] gradient_u;
        if (user % (num_users / 300) == 0)
            std::cout << "Finished " << user << " / " << num_users << " users" << std::endl;
    }
    
    // Now repeat for V
    for(int movie = 0; movie < num_movies; movie++) {
        // double* curr_u = U.get(user);
        double* gradient_v = new double[k]();
        // sum up all yij(
        for (int i = 0; i < num_users; i++) {
            int yij = data->get(i, movie);
            if (yij > 0) {
                // Then yij exists, so let's update
                
                double* u_i = U->get_row(i);
                
                // compute err = y - u_i^T * v_j
                // recall that predict_rating(i,j) = u_i^T * v_j
                double err = yij - predict_rating(i, movie, U, V);
                
                // now add to the gradient
                for (int col = 0; col < k; col++) {
                    gradient_v[col] -= 2 * err * delta * u_i[col];
                }
            }
        }
        // do update, free gradient_u
        V->update(movie, gradient_v, delta);
        delete[] gradient_v;
    }
}


int* parse_line(std::string line) {
    int* values = new int[3];
    int prev_index = 0;  // start of string
    int num = 0;         // index of values
    for (int i = 0; i < line.size(); i++) {
        if (line[i] == ' ') {
            // get substring
            /*char substring[i - prev_index + 2];
            memcpy(substring, &line[prev_index], i - prev_index + 1);
            substring[i - prev_index + 1] = '\0';
            */
            std::string substring = line.substr(prev_index, i - prev_index + 1);
            
            // convert it to a num
            values[num] = std::stoi(substring, nullptr, 10);
            
            num++;
            prev_index = i + 1;
        }
        if (num == 3)
            break;
    }
    values[2] = std::stoi(line.substr(prev_index, line.size() - prev_index), nullptr, 10);
    return values;
}

int main(int argc, char **argv)
{
    /*
    assert(argc == 2);
    char* filename = argv[1];
    */
    // For now, let's just set filename and allow no inputs
    std::ifstream file("/home/bryant/tmp.mm");
    
    SparseMatrix* data = new SparseMatrix();
    
    // Now we're loading the data
    
    const int MAX_CHARS_PER_LINE = 100;
    int count = 0;
    // Might as well store these values since they're in the file
    int num_users;
    int num_movies;
    int num_elements;
    
    // loading data
    std::cout << "Reading file..." << std::endl;
    
    std::string line;
    while (std::getline(file, line)) {
        /*char buf[MAX_CHARS_PER_LINE];
        file.getline(buf, MAX_CHARS_PER_LINE);
        std::cout << count << std::endl;
        std::cout << buf << std::endl;*/
        
        if (count == 0) {
            // reading the header, just continue
            count++;
            continue;
        } else if (count == 1) {
            // read in the number of users, movies, and entries
            //int* values = parse_line(buf);
            int* values = parse_line(line);
            num_users = values[0];
            num_movies = values[1];
            num_elements = values[2];
            std::cout << num_users << ", " << num_movies << ", " << num_elements << std::endl;
        } else {
            //int* values = parse_line(buf);
            int* values = parse_line(line);
            data->addRating(values[0], values[1], (unsigned short)values[2]);
            if (count % (num_elements/25) == 0) {
                std::cout << "Done with " << count << " / " << num_elements << std::endl;
            }
        }
        count++;
    }
    file.close();
    
    // now that data is loaded, sort it
    data->sort();
    
    std::cout << "Done sorting!" << std::endl;
    
    int k = 25; // works well for now, later we'll regularize
    
    // Initialize U and V to random values
    Matrix* U = new Matrix(num_users, k);
    Matrix* V = new Matrix(num_movies, k);
    
    std::cout << "Done initializing U and V!" << std::endl;
    
    int max_iters = 1000;
    // call update_sgd for all i, j for max_iter iterations
    // or until some criteria w/ fnorm
    int iter = 0;
    while (iter < max_iters) {
        update_sgd(U, V, data);
        std::cout << "Done with iter " << iter << std::endl;
        iter++;
    }
    // Now write out U and V and make predictions
    std::ofstream outputU("U.coords");
    outputU << U->to_string();
    outputU.close();
    
    std::ofstream outputV("V.coords");
    outputV << V->to_string();
    outputV.close();
    
    
    
    return 0;
}

