#include "SparseMatrix.hh"
#include <algorithm>
#include <iostream>

SparseMatrix::SparseMatrix() {
    // TODO: any initiation stuff
    
}

SparseMatrix::~SparseMatrix() {
    // TODO: any cleanup
}

void SparseMatrix::addRating(int user, int movie, unsigned short rating) {
    Rating new_rating = {user, movie, rating};
    ratings.push_back(new_rating);
}

void SparseMatrix::sort() {
    std::sort(ratings.begin(), ratings.end(), sortObj);
}

bool SparseMatrix::compare(Rating r, int user, int movie) {
    if (r.user < user) 
        return -1;
    else if (r.user > user)
        return 1;
    else {
        if (r.movie < movie)
            return -1;
        else if (r.movie > movie)
            return 1;
        else
            return 0;
    }
}

/**
 * Uses a binary search to find the element.  Uses the 
 * 'compare' helper function.
 */
int SparseMatrix::get(int user, int movie) {
    int min = 0;
    int max = ratings.size();
    int index = (min + max) / 2;
    Rating curr_elem = ratings[index];
    while (max > min + 1 && compare(curr_elem, user, movie) != 0) {
        if (compare(curr_elem, user, movie) < 0)
            // then go to bottom half
            max = index;
        else
            min = index;
        index = (min + max) / 2;
        curr_elem = ratings[index];
        //std::cout << min << ", " << max << ", " << index << std::endl;
    }
    
    // return if (user, movie) is in ratings, otherwise
    // return -1
    if (compare(curr_elem, user, movie) == 0)
        return curr_elem.rating;
    else if (compare(ratings[index+1], user, movie) == 0)
        return ratings[index+1].rating;
    
    return -1;
}
